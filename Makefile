# -*- indent-tabs-mode:t; -*-

CC=clang
NUM_HASHES=100
ROCKYOU=rockyou100.txt
#ROCKYOU=rockyou1m.txt
BOWFILE=rainbow.bow

all: hashpass crack rainbow dump

# This rule links hashpass.o and md5.o along with the
# libssl and libcrypto libraries to make the executable.
hashpass: hashpass.o md5.o
	clang hashpass.o md5.o -o hashpass -l ssl -l crypto

md5.o: md5.c md5.h
	clang -g -c md5.c -Wall

hashpass.o: hashpass.c md5.h
	clang -g -c hashpass.c -Wall

crack: crack.o md5.o
	clang crack.o md5.o -o crack -l ssl -l crypto

crack.o: crack.c md5.h
	clang -g -c crack.c -Wall

hashes: hashpass $(ROCKYOU)
	shuf -n $(NUM_HASHES) $(ROCKYOU) > passwords.txt
	./hashpass < passwords.txt > hashes.txt

rainbow: rainbow.o md5.o
	clang rainbow.o md5.o -o rainbow -l ssl -l crypto

rainbow.o: rainbow.c
	clang -g -c rainbow.c -Wall

$(BOWFILE): rainbow $(ROCKYOU)
	./rainbow $(ROCKYOU) $(BOWFILE)

dump: dump.c md5.o
	clang -g dump.c md5.o -o dump -l ssl -l crypto

# Fetch the rockyou.txt file that contains 3 million entries
rockyou3m.txt:
	wget http://cs.sierracollege.edu/cs46/rockyou3m.txt.gz
	gunzip rockyou3m.txt.gz

# Make the 1 million entry rockyou file
rockyou1m.txt: rockyou3m.txt
	head -n 1000000 rockyou3m.txt > rockyou1m.txt

clean:
	rm -f *.o *.bow dump rainbow hashpass crack hashes.txt passwords.txt rockyou3m.txt rockyou1m.txt

test: crack $(BOWFILE)
	./crack hashes.txt $(BOWFILE)
