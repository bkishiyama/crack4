#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "entry.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

//function to get an index
int start(unsigned char* digest)
{
//Extract the first three bytes
//Use those three bytes as a 24-bit number
int start = digest[0] * 65536 + digest[1] * 256 + digest[2];
return start;
}


void rainbow_crack(char *hashfile, char *rainbow)
{
    //Open the text hashes file for reading
    FILE *f = fopen(hashfile, "r");
    //Open the binary rainbow file for reading
    FILE *rf = fopen(rainbow, "rb");
    //Check to make sure file was successfully opened
    if (!f)
    {
        printf("Can't open hashes file for reading\n");
        exit(1);
    }
    // Check to make sure if was successfully opened
    if (!rf)
    {
        printf("Can't open rainbow file for reading\n");
        exit(1);
    }
    // For each hash (read one line at a time out of the file):
    char hash[HASH_LEN];
    while(fscanf(f, "%s\n", hash)!= EOF)
    {
        //Convert hex string to digest (use hex2digest. See md5.h)
        unsigned char *digest=hex2digest(hash);
        //Extract the first three bytes
        int index = start(digest);
        //Seek to that location in the rainbow file
        int ilocation = index*sizeof(entry);
        fseek(rf,ilocation,SEEK_SET); 
        //Read the entry found there 
        //Check to see if it's the one you want
        //If not, read the next one
        // Repeat until you find the one you want.
        struct entry e;
        int end = 0;
        do 
        {
            fread(&e,sizeof(struct entry),1,rf);
            if(memcmp(hash, digest2hex(e.hash), 16)==0)
            {
                //   Display the hash and the plaintext
                printf("%s %s\n", hash, e.pass);
                end =1;

            }
        }
        while(end != 1);
    }
    //close files
    fclose(rf);
    fclose(f);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file rainbow_file\n", argv[0]);
        exit(1);
    }
    //function that does basically everything in the assignment
    rainbow_crack(argv[1], argv[2]);
}
